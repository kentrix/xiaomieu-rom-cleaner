#!/bin/bash

set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

TEMPDIR="${DIR}/cleaner-${RANDOM}"
MNT_PT="${TEMPDIR}-MNT/"
BROMITE_RELEASE='79.0.3945.139'
HACKER_KEYBORD_RELEASE='1.40.7'

REMOVAL_REGEX=('*[Gg]oogle*' '*[Gg]ms*')
REMOVAL_LIST=('Velvet')

trap cleanup 0 1 2 3 6 15

cleanup() {
    echo "Caught signal ... Cleaning up"
    rm -rf "$TEMPDIR" || :
    rm -rf "$MNT_PT" || :
    echo "Cleaned"
}

do_remove() {
    cd "$MNT_PT"

    echo 'Removing...'

    rm -rf 'META-INF/com/google/android/'

    # Remove APKs
    find . -regextype egrep -regex '.*/.*([Gg]oogle|[Gg]ms|[Gg]sf|Velvet|Phonesky|Vending).*' \
        -type 'd' -printf 'Removing %f\n' -prune -exec rm -rf {} \;

    # Remove jars and xmls
    find . -regextype egrep -regex '.*/.*([Gg]oogle|[Gg]ms|[Gg]sf|Velvet|Phonesky|Vending).*\.(xml|jar)' \
        -type 'f' -printf 'Remvoing %f\n' -prune -exec rm -rf {} \;

    sed -i '/gpay/d' 'init.miui.rc'

}

main() {
    mkdir -p "$TEMPDIR"
    mkdir -p "$MNT_PT"
    unzip "$1" -d "$TEMPDIR"
    cd "$TEMPDIR"
    # Extract br file
    echo 'Extracting brotli file'
    brotli -d 'system.new.dat.br'

    # sdat2img to convert dat to ext4 image
    echo 'Converting dat to ext4 image'
    python "${DIR}/sdat2img.py" 'system.transfer.list' 'system.new.dat' 'system.img' > /dev/null

    echo 'Mounting image...'
    # Mountable ext4 image
    mount -o loop -t ext4 'system.img' "$MNT_PT"

    do_remove

    cd "$TEMPDIR"
    umount "$MNT_PT"

    # Repack the image
    python "${DIR}/rimg2sdat.py" -v 4 'system.img' > /dev/null

    # Compress with brotli
    echo 'Compressing to br...'
    rm -rf 'system.new.dat.br'
    brotli -q 1 'system.new.dat'

    echo 'Cleaning up files...'
    rm -rf 'system.img' 'system.new.dat'

    echo 'Zipping...'
    zip -r "${DIR}/../$(basename "$1")-cleaned.zip" "${TEMPDIR}/."

    echo 'Done'

}


if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

if [[ -z "$1" ]]; then
    echo "Usage: $0 <zipfile>"
else
    main "$@"
fi
